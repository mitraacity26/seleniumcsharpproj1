﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using xl = Microsoft.Office.Interop.Excel;
namespace ClassLibrary_new
{
    public class Utility : Objects
    {
        static xl.Application xlApp = null;
        static xl.Workbooks workbooks = null;
        static xl.Workbook workbook = null;
        static Hashtable sheets;
        public static string xlFilePath = @"C:\Users\pnl08i34\source\repos\ClassLibrary_new\ClassLibrary_new\Resources\datasheet.xlsx";
        static IWebElement ele;
        public static IWebElement Access_by_Locator(IWebDriver driver, String type_of_locator, String locator)
        {




            switch (type_of_locator.ToLower())
            {
                case "xpath":
                    ele = driver.FindElement(By.XPath(locator));
                    break;
                case "id":
                    ele = driver.FindElement(By.Id(locator));
                    break;
                case "class":
                    ele = driver.FindElement(By.ClassName(locator));
                    break;
                default:
                    Console.WriteLine("Locator is unknown.");
                    break;
            }
            return ele;
        }



        //public Utility(string xlFilePath)
        //{
        //   this.xlFilePath = xlFilePath;
        //}

        public static void OpenExcel()
        {
            xlApp = new xl.Application();
            workbooks = xlApp.Workbooks;
            workbook = workbooks.Open(xlFilePath);
            sheets = new Hashtable();
            int count = 1;
            // Storing worksheet names in Hashtable.
            foreach (xl.Worksheet sheet in workbook.Sheets)
            {
                sheets[count] = sheet.Name;
                count++;
            }
        }

        public static void CloseExcel()
        {
            workbook.Close(false, xlFilePath, null); // Close the connection to workbook
            Marshal.FinalReleaseComObject(workbook); // Release unmanaged object references.
            workbook = null;

            workbooks.Close();
            Marshal.FinalReleaseComObject(workbooks);
            workbooks = null;

            xlApp.Quit();
            Marshal.FinalReleaseComObject(xlApp);
            xlApp = null;
        }
        public static string GetCellData(string sheetName, int colNumber, int rowNumber)
        {
            OpenExcel();

            string value = string.Empty;
            int sheetValue = 0;

            if (sheets.ContainsValue(sheetName))
            {
                foreach (DictionaryEntry sheet in sheets)
                {
                    if (sheet.Value.Equals(sheetName))
                    {
                        sheetValue = (int)sheet.Key;
                    }
                }
                xl.Worksheet worksheet = null;
                worksheet = workbook.Worksheets[sheetValue] as xl.Worksheet;
                xl.Range range = worksheet.UsedRange;

                value = Convert.ToString((range.Cells[rowNumber, colNumber] as xl.Range).Value2);
                Marshal.FinalReleaseComObject(worksheet);
                worksheet = null;
            }
            CloseExcel();
            return value;
        }


        public static String ReadConfig(String key)

        {
            
            return ConfigurationManager.AppSettings.Get(key);

        }


       

    }

}
