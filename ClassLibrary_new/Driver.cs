﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary_new
{
    public class Driver
    {
        public static IWebDriver driver;

        public static void DriverMethod()
        {


            switch (Utility.ReadConfig("Browser").ToLower())
            {
                case "chrome":
                    driver = new ChromeDriver();
                    break;
                case "ie":
                    driver = new FirefoxDriver();
                    break;
                case "class":
                    driver = new InternetExplorerDriver();
                    break;
                default:
                    Console.WriteLine("Browser is unknown");
                    break;
            }
            //return driver;
        }
    }
}
