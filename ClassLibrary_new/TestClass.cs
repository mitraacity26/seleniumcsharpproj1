﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ClassLibrary_new
{

    public class TestClass : Objects
    {
        public static IWebDriver driver;
        [SetUp]
        public static void SetupMethod()
        {
            // driver = new ChromeDriver();
            Driver.DriverMethod();
            Driver.driver.Navigate().GoToUrl(Utility.ReadConfig("AppURL"));
            Driver.driver.Manage().Window.Maximize();
            
          
        }

        [Test]
        public static void TestCase1()
        {
            Application_Methods.SearchGoogle();

        }

        [TearDown]
        public static void TeardownMethod()
        {
            Driver.driver.Quit();
        }
    }
}
